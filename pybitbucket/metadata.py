# -*- coding: utf-8 -*-
"""Project metadata

Information describing the project.
"""

# The package name, which is also the "UNIX name" for the project.
package = 'pybitbucket'
project = "PyBitbucket"
project_no_spaces = project.replace(' ', '')
version = '0.3.0'
description = 'A Python wrapper for the Bitbucket API'
authors = ['Ian Buchanan']
authors_string = ', '.join(authors)
emails = ['ibuchanan@atlassian.com']
license = 'MIT'
copyright = '2015 ' + authors_string
url = 'https://bitbucket.org/ian_buchanan/python-bitbucket'
